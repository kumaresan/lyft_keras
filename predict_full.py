import os
from skimage.transform import resize
from skimage.io import imsave
import numpy as np
import sys, skvideo.io, json, base64
from PIL import Image
from io import BytesIO, StringIO
from keras.models import Model
from keras.layers import Input, concatenate, Conv2D, MaxPooling2D, Conv2DTranspose
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
from keras import backend as K

#import matplotlib.pyplot as plt
#import matplotlib.image as mpimg
#%matplotlib inline

K.set_image_data_format('channels_last')

#file = 'test_video.mp4'
file = sys.argv[-1]

if file == 'predict.py':
  print ("Error loading video")
  quit

# Define encoder function
def encode(array):
    pil_img = Image.fromarray(array)
    buff = BytesIO()
    pil_img.save(buff, format="PNG")
    return base64.b64encode(buff.getvalue()).decode("utf-8")

video = skvideo.io.vread(file)

answer_key = {}

########################################################################################
# Define Model and Loss

smooth = 1.

def dice_coef(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)


def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)


# define model 
img_rows = 320
img_cols = 800


def get_unet():
    inputs = Input((img_rows, img_cols, 3))
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(inputs)
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool1)
    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(pool2)
    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

    conv4 = Conv2D(256, (3, 3), activation='relu', padding='same')(pool3)
    conv4 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)

    conv5 = Conv2D(512, (3, 3), activation='relu', padding='same')(pool4)
    conv5 = Conv2D(512, (3, 3), activation='relu', padding='same')(conv5)

    up6 = concatenate([Conv2DTranspose(256, (2, 2), strides=(2, 2), padding='same')(conv5), conv4], axis=3)
    conv6 = Conv2D(256, (3, 3), activation='relu', padding='same')(up6)
    conv6 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv6)

    up7 = concatenate([Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(conv6), conv3], axis=3)
    conv7 = Conv2D(128, (3, 3), activation='relu', padding='same')(up7)
    conv7 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv7)

    up8 = concatenate([Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(conv7), conv2], axis=3)
    conv8 = Conv2D(64, (3, 3), activation='relu', padding='same')(up8)
    conv8 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv8)

    up9 = concatenate([Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(conv8), conv1], axis=3)
    conv9 = Conv2D(32, (3, 3), activation='relu', padding='same')(up9)
    conv9 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv9)

    conv10 = Conv2D(1, (1, 1), activation='sigmoid')(conv9)

    model = Model(inputs=[inputs], outputs=[conv10])

    model.compile(optimizer=Adam(lr=1e-5), loss=dice_coef_loss, metrics=[dice_coef])
    #model.compile(optimizer=Adam(lr=1e-5), loss='categorical_crossentropy', metrics=['accuracy'])

    return model

########################################################################################
#Load Models

road_model = get_unet()
road_model.load_weights('road_weights.h5')

car_model = get_unet()
car_model.load_weights('car_weights.h5')

########################################################################################
#Predict 

# Frame numbering starts at 1
frame = 1

for image in video:

    #prepare inputs
    input_image = np.array(image).astype('float32')
    input_image = (input_image - 128) / 128

    road_input_image = input_image[200:520,:,:]
    road_input_image = road_input_image.reshape((1,320,800,3))  

    car_input_image = input_image[175:495,:,:]
    car_input_image = car_input_image.reshape((1,320,800,3))  

    #predict
    road_pred = road_model.predict(road_input_image)
    car_pred = car_model.predict(car_input_image)
    
    #post process output
    road_pred = road_pred[0,:,:,0]
    binary_road_result = np.concatenate((np.zeros((200,800)),road_pred))
    binary_road_result = np.concatenate((binary_road_result,np.zeros((80,800))))
    binary_road_result = np.where(binary_road_result>0.5,1,0).astype('uint8')
    
    car_pred = car_pred[0,:,:,0]
    binary_car_result = np.concatenate((np.zeros((175,800)),car_pred))
    binary_car_result = np.concatenate((binary_car_result,np.zeros((105,800))))
    binary_car_result = np.where(binary_car_result>0.5,1,0).astype('uint8')
    
    masked_image = np.copy(image)
    masked_image[:,:,1] = masked_image[:,:,1] + binary_road_result*100
    masked_image[:,:,2] = masked_image[:,:,2] + binary_car_result*200


#    fig=plt.figure(figsize=(15, 15))
#    row = 2
#    col = 2
#
#    fig.add_subplot(row, col, 1)
#    plt.imshow(image) 
#
#    fig.add_subplot(row, col, 2)
#    plt.imshow(masked_image)
#    
#    fig.add_subplot(row, col, 3)
#    plt.imshow(binary_car_result) 
#
#    fig.add_subplot(row, col, 4)
#    plt.imshow(binary_road_result)
    
    answer_key[frame] = [encode(binary_car_result), encode(binary_road_result)]
    
    # Increment frame
    frame+=1

# Print output in proper json format
print (json.dumps(answer_key))

