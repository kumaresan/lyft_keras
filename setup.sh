mkdir ../data

cd ../data
wget https://s3-us-west-1.amazonaws.com/udacity-selfdrivingcar/Lyft_Challenge/Training+Data/lyft_training_data.tar.gz 
tar -xzf lyft_training_data.tar.gz


git config --global user.email "kumaresan.manickavelu@gmail.com"
git config --global user.name "Kumaresan"

conda install -c anaconda scikit-image 
conda install -c conda-forge sk-video
conda install -c anaconda tensorflow-gpu
conda install -c anaconda keras-gpu
conda install -c anaconda imageio
conda install -c conda-forge moviepy 
